console.log("all systems go!")

/*
Challenge 1: Making Teachers Fancy
STEP 1: Turn teacher rating variables into an array
*/
var teacher = "Sally Jones";
var department = "Physics";
var ratings = [3.4, 5.0, 4.2];
/*
STEP 2: Turn teacher’s avg rating calculation into a function
Create a function called getRatingAvg that takes one parameter, an array of ratings
and return the average rating.
*/
function getRatingAvg(arrayRatings) {
    var total = 0
    for (var i = 0; i < arrayRatings.length; i++) {
        total += arrayRatings[i]
    }
    var avgRating = total / arrayRatings.length
    return Math.round(avgRating * 10) / 10
}
// var avgRating = getRatingAvg(ratings)
// console.log(avgRating)
/*
STEP 3: Create an add teacher rating function
Create function called addTeacherRating, that takes a ratings array and a new rating.
Add the new rating to the ratings array and return the array
*/
function addTeacherRating(arrayRatings, newRating) {
    arrayRatings.push(parseFloat(newRating))
    return arrayRatings
}
// console.log(`ratings ${ratings}`)
// var newArray = addTeacherRating(ratings, 3.7)
// console.log(`newArray ${newArray}`)
/*
STEP 4: Putting All the Pieces Together
Create a prompt that asks the user to review a teacher
1. check that the user entered a number between 0.0 and 5.0… 
2. if they did not, prompt them again… 
3. if they did, add the rating value to the teacher’s rating array 
4. AND alert back to the user
*/
// var teacherName = `Sally Jones`
// var teacherRatings = ratings
// var teacherAverage = getRatingAvg(teacherRatings)

// var teacherReviewMessage = `We would like for you to review . Please enter a rating between 0.0 - 5.0?`
// var teacherReview = prompt(teacherReviewMessage)

// while (teacherReview > 5) {
//     teacherReview = prompt(`${teacherReviewMessage}. Please choose between 0.0 and 5.0.`)
// }

// teacherRatings = addTeacherRating(teacherRatings, teacherReview)
// teacherAverage = getRatingAvg(teacherRatings)
// var teacherThankYou = `Thanks for you review! ${teacherName} average rating is now ${teacherAverage}.`
// // console.log(teacherThankYou)
// alert(teacherThankYou)

/*
Challenge 2: Making Courses Fancy
STEP 1: Create a courses array
[
    [<course-title>, <course-department>],
    [<course-title>, <course-department>],
    ..etc
]
*/
var coursesArray = [
    [ 'Basketball', 'Physical Education'],
    [ 'Softball', 'Physical Education'],
    [ 'Volleyball', 'Physical Education'],
    [ 'Drawing', 'Art'],
    [ 'Painting', 'Art'],
    [ 'Ceramics', 'Art'],
    [ 'World War I', 'History'],
    [ 'World War II', 'History'],
    [ 'War on Drugs', 'History'],
]
/*
STEP 2: Create a function that filters course by departments
1. take 2 parameter a course array and a department. 
2. return a new array filled with courses that are ONLY in the department specified in the parameter.
*/
function filterCourses(arrayCourses, department) {
    return arrayCourses.filter(course => course[1].toLowerCase() == department.toLowerCase())
}
// var filtered = filterCourses(coursesArray, "art")
// console.log('filtered')
// console.log(filtered)
/*
STEP 3: Putting it all together
Prompt the user what department they are looking for a course in
1. Check that the user entered a valid department name
2. if they did not, prompt them again
3. if they did, use the function you create above to filter the course list 
4) Alert the user the course titles that they can choose from.
*/
var departmentPromptMessage = `What department are you looking for a course in?`
var department = prompt(departmentPromptMessage)

var validDepartments = []
coursesArray.forEach(course => validDepartments.push(course[1]))
validDepartments = validDepartments.filter(function (elem, pos) {
    return validDepartments.indexOf(elem) == pos;
});
var validDepartmentsMessage = `\nPlease choose from: ${validDepartments.join(`, `)}`
var validDepartmentsLower = []
validDepartments.forEach(dept => validDepartmentsLower.push(dept.toLowerCase()))
while (validDepartmentsLower.indexOf(department) == -1) {
    department = prompt(`${departmentPromptMessage} ${validDepartmentsMessage}`)
}
var filteredCourses = filterCourses(coursesArray, department)
var availableCourses = []
filteredCourses.forEach(course => availableCourses.push(course[0]))
var availableCoursesMessage = `These courses are available in that department: ${availableCourses.join(`, `)}.`
// console.log(availableCoursesMessage)
alert(availableCoursesMessage)
