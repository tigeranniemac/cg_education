// Challenge 1: Create and Link a Javascript File.
console.log("all systems go!")

/*
Challenge 2: Playing with Variables.
Print out 3 variables like:
Teacher: Sally Jones
Department: Physics
Ratings: 3.4, 5.0, 4.2
*/

// var teacher = "Sally Jones";
// var department = "Physics";
// var ratings = [3.4, 5.0, 4.2];

// console.log(`Teacher: ${teacher}`)
// console.log(`Department: ${department}`)
// console.log(`Ratings: ${ratings[0]}, ${ratings[1]}, ${ratings[2]}`)

/*
Challenge 3: Do a little math.
Average the teacher ratings into var avgRating
var avgRating round to near 10th place decimal
//One decimal place to the right of the decimal place is the tenths place.
Teacher: Sally Jones
Department: Physics
Ratings: 3.4, 5.0, 4.2
Avg Rating: 4.4
*/

var teacher = "Sally Jones";
var department = "Physics";
var ratings = [3.4, 5.0, 4.2];
var avgRating = (ratings[0] + ratings[1] + ratings[2]) / 3
var avgDecimal = Math.round(avgRating).toFixed(1)

console.log(`Teacher: ${teacher}`)
console.log(`Department: ${department}`)
console.log(`Ratings: ${ratings[0]}, ${ratings[1]}, ${ratings[2]}`)
console.log(`Average Rating: ${avgDecimal}`)

/*
Challenge 4: Variables to Represent Data!
Then, make a list of data used on those pages and create variables to represent them. 
Make sure the data type you use… like string or int or float… best presents the item.
After you create the variables… create nice print statements :)
*/

var student = "Sally Jones";
var major = "Physics";
var email = "sallyjones@gmail.com";
var gpa = 5.0;
var courses = ["Astronomy", "Physics"];

console.log(`Student: ${student}`)
console.log(`Major: ${major}`)
console.log(`Email: ${email}`)
console.log(`GPA: ${gpa}`)
console.log(`Courses: ${courses[0]}, ${courses[1]}`)

var classTitle = "Astronomy";
var department = "Physics";
var teacher = "Sally Jones";
var semester = "Fall 2017";

console.log(`Class: ${classTitle}`)
console.log(`Department: ${department}`)
console.log(`Teacher: ${teacher}`)
console.log(`Semester: ${semester}`)