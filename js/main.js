console.log("all systems go!")
/*
    Create Prototypes : Teacher, Course, Student
*/
//Teacher Object Prototype
function Teacher(name, department, ratings) {
    this.name = name
    this.department = department
    this.ratings = ratings
    this.avgRating = 0
    this.courses = []
}
Teacher.prototype.addRating = function (newRating) {
    this.ratings.push(parseFloat(newRating))
}
Teacher.prototype.addCourse = function (newCourse) {
    this.courses.push(newCourse)
}
Teacher.prototype.getAvgRating = function () {
    this.avgRatingOld = this.avgRating
    let total = 0
    for (let i = 0; i < this.ratings.length; i++) {
        total += this.ratings[i]
    }
    let avgRating = total / this.ratings.length
    avgRating = Math.round(avgRating * 10) / 10
    this.avgRating = avgRating
}
Teacher.prototype.niceTeacher = function (teacherReview) {
    let niceTeacher = `${this.name} 
${this.department} 
Avg. Rating: ${this.avgRatingOld}
Adding Rating: ${teacherReview}
New Avg Rating: ${this.avgRating}`
    console.log(niceTeacher)
}
//Course Object Prototype
function Course(name, department, teacher, semester) {
    this.name = name
    this.department = department
    this.teacher = teacher
    this.semester = semester
    this.students = []
}
Course.prototype.addStudent = function (student) {
    this.students.push(student)
}
//Student Object Prototype
function Student(name, major, email, gpa, courses) {
    this.name = name
    this.major = major
    this.email = email
    this.gpa = gpa
    this.courses = courses
}

/*
    Init Data : Teacher, Courses
*/
let coursesArray = []
let teachersArray = []
let studentsArray = []

let patSummit = new Teacher('Pat Summitt', 'Basketball', [5.0, 5.0, 5.0]);
patSummit.getAvgRating()
teachersArray.push(patSummit)
let introBasketball = new Course('Intro to Basketball', 'Physical Education', patSummit, 'Fall')
patSummit.addCourse('Intro to Basketball')
coursesArray.push(introBasketball)
let jvBasketball = new Course('JV Basketball', 'Physical Education', patSummit, 'Spring')
coursesArray.push(jvBasketball)
patSummit.addCourse('JV Basketball')
let varsityBasketball = new Course('Varsity Basketball', 'Physical Education', patSummit, 'Fall')
coursesArray.push(varsityBasketball)
let proBasketball = new Course('Pro Basketball', 'Physical Education', patSummit, 'Spring')
coursesArray.push(proBasketball)

let bobRoss = new Teacher('Bob Ross', 'Art', [3.4, 5.0, 4.2])
teachersArray.push(bobRoss)
bobRoss.getAvgRating()
let drawing = new Course('Drawing', 'Art', bobRoss, 'Fall')
coursesArray.push(drawing)
bobRoss.addCourse('Drawing')
let painting = new Course('Painting', 'Art', bobRoss, 'Spring')
coursesArray.push(painting)
bobRoss.addCourse('Painting')
let sculpture = new Course('Sculpture', 'Art', bobRoss, 'Fall')
coursesArray.push(sculpture)

let tinaFey = new Teacher('Tina Fey', 'Comedy Writing', [5.0, 4.3, 3.7])
teachersArray.push(tinaFey)
tinaFey.getAvgRating()
tinaFey.addCourse('30 Rock')
tinaFey.addCourse('Mean Girls')
let leslieKnope = new Teacher('Leslie Knope', 'Civil Service', [2.4, 4.9, 3.8])
teachersArray.push(leslieKnope)
leslieKnope.getAvgRating()
leslieKnope.addCourse('Volunteering')
leslieKnope.addCourse('Election Campaigning')

let sallyJones = new Student('Sally Jones', 'Physical Education', 'sally@jones.com', 3.4, ['Mean Girls', 'Intro to Basketball'])
studentsArray.push(sallyJones)
let sallyJones1 = new Student('Kelly Jones', 'Art', 'kelly@jones.com', 4.2, ['Painting', 'Volunteering'])
studentsArray.push(sallyJones1)
let sallyJones2 = new Student('Jessica Jones', 'Comedy Writing', 'jessica@jones.com', 4.8, ['30 Rock', 'Varsity Basketball'])
studentsArray.push(sallyJones2)

// console.log(coursesArray)
// console.log(teachersArray)
// console.log(studentsArray)


/*
    Helper Functions
*/
function toggleLoading() {
    let spinner = document.querySelector('.spinner')
    let mainContent = document.querySelector('.cg_education__content')
    spinner.classList.toggle('fadeout')
    setTimeout(function () {
        mainContent.classList.toggle('fadein')
    }, 1000)
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
    // console.log(email)
    // var simpleTest = email.includes('@')
    // console.log(simpleTest)
    // return simpleTest
}

/*
    Teacher Related Functions
*/
function updateTeachersDisplay(teachers, highlightTeacher) {
    let teachers_container = document.querySelector('.teachers_list')
    teachers_container.innerHTML = ''
    // console.log('List of teachers')
    for (let i = 0; i < teachers.length; i++) {
        // console.log(`${teachers[i].name}, ${teachers[i].department}`)
        let updatedTeacher = highlightTeacher == teachers[i].name ?  ' updated' : ''
        let content = `
            <article class="teacher${updatedTeacher}">
                <header class="teacher__header">
                    <a href="javascript:void(0)" data-teacher="${teachers[i].name}"><img class="teacher__thumbnail thumbnail" src="img/profile_thumb.png" alt="${teachers[i].name}" /></a>
                    <aside class="teacher__name_dept">
                        <h2 class="teacher__name"><a href="javascript:void(0)" data-teacher="${teachers[i].name}">${teachers[i].name}</a></h2>
                        <h3 class="teacher__department">${teachers[i].department}</h3>
                    </aside>
                </header>
                <section class="teacher_info">
                    <aside class="teacher__teachers">
                        <h4 class="teacher__courses_title">Courses</h4>
                        <ul class="teacher__courses_list course_list">
                            <li>${teachers[i].courses[0]}</li>
                            <li>${teachers[i].courses[1]}</li>
                        </ul>
                    </aside>
                    <aside class="teacher__rating">
                        <h4 class="teacher__rating_title">Rating</h4>
                        <p class="teacher__rating_result"><span class="active-rating">${teachers[i].avgRating}</span> <span class="total-rating">5.0</span></p>
                    </aside>
                </section>
            </article>`
        teachers_container.innerHTML += content
    }
    let teacherLinks = document.querySelectorAll(".teacher a");
    for (let i = 0; i < teacherLinks.length; i++) {
        teacherLinks[i].addEventListener("click", showTeacherForm)
    }
    document.querySelector('.teachers_form .close-button').addEventListener("click", hideTeacherForm)
}

let current_teacher
function formTeacherSubmission(e) {
    e.preventDefault()
    const ratingInput = document.getElementById('new_rating')
    const ratingValue = ratingInput.value
    if (ratingValue == '') {
        alert('Please enter a rating between 0.0 and 5.0.')
        ratingInput.classList.add('error')
        return
    }
    if (ratingValue > 5 || isNaN(ratingValue)) {
        alert('Please enter a rating less than 5.0.')
        ratingInput.classList.add('error')
        return
    }
    if (ratingValue < 0 || isNaN(ratingValue)) {
        alert('Please enter a rating more than 0.0.')
        ratingInput.classList.add('error')
        return
    }
    const submitButton = document.querySelector('.submit_button')
    const submitValue = submitButton.dataset.teacher
    let loadedTeacher = teachersArray.find(item => item.name === submitValue)
    loadedTeacher.addRating(ratingValue)
    loadedTeacher.getAvgRating()
    updateTeachersDisplay(teachersArray, loadedTeacher.name)
    ratingInput.value = ""
    let teacherForm = document.querySelector('.teachers_form')
    teacherForm.classList.remove('show-panel')
    current_teacher = ''
}
function hideTeacherForm(e) {
    let teacherForm = document.querySelector('.teachers_form')
    teacherForm.classList.remove('show-panel')
    current_teacher = ''
}
function showTeacherForm(e) {
    e.preventDefault()
    // console.log(current_teacher)
    let teacherForm = document.querySelector('.teachers_form')
    if (current_teacher && current_teacher == this.dataset.teacher) {
        teacherForm.classList.remove('show-panel')
        current_teacher = ''
        return
    }
    current_teacher = this.dataset.teacher
    let submitButton = document.querySelector('#form_teachers_rating .submit_button')
    submitButton.dataset.teacher = this.dataset.teacher
    let loadedTeacher = teachersArray.find(item => item.name === current_teacher)
    let teacherName = document.getElementById('teacher_name')
    teacherName.innerHTML = loadedTeacher.name
    let teacherDepartment = document.getElementById('teacher_department')
    teacherDepartment.innerHTML = loadedTeacher.department
    let teacherRating = document.getElementById('teacher_rating')
    teacherRating.innerHTML = loadedTeacher.avgRating
    teacherForm.classList.add('show-panel')
}

/*
    Course Related Functions
*/
function updateCourseDisplay(courses) {
    let courses_container = document.querySelector('.courses_list')
    courses_container.innerHTML = ''
    // console.log('List of Courses')
    for (let i = 0; i < courses.length; i++) {
        // console.log(`${courses[i].name}, ${courses[i].department}`)
        let link = courses[0].name.toLowerCase().split(' ').join('-');
        let content = `<article class="course">
                <aside class="course__name_dept">
                    <h4 class="course__name"><a href="courses/${link}.html">${courses[i].name}</a></h4>
                    <h4 class="course__dept">${courses[i].department}</h4>
                </aside>
                <aside class="course__teacher_semester">
                    <div class="course__detail_row detail_row">
                        <h4 class="course__teacher_title">Teacher:</h4>
                        <p class="course__teacher">${courses[i].teacher.name}</p>
                    </div>
                    <div class="course__detail_row detail_row">
                        <h4 class="course__semester_title">Semester:</h4>
                        <p class="course__semester">${courses[i].semester}</p>
                    </div>
                </aside>
            </article>`
        courses_container.innerHTML += content
    }
}

function updateSelectDepartments(courses) {
    let validDepartments = []
    courses.forEach(course => validDepartments.push(course.department))
    validDepartments = validDepartments.filter(function (elem, pos) {
        return validDepartments.indexOf(elem) == pos;
    });
    let select_department = document.querySelector('#select_department') ? document.querySelector('#select_department') : document.querySelector('#student_major')
    console.log('Filter Departments')
    console.log(select_department)
    for (let i = 0; i < validDepartments.length; i++) {
        console.log(`${validDepartments[i]}`)
        let content = `<option value="${validDepartments[i]}">${validDepartments[i]}</option>`
        select_department.innerHTML += content
    }
}
function updateSelectSemesters(courses) {
    let validSemesters = []
    courses.forEach(course => validSemesters.push(course.semester))
    validSemesters = validSemesters.filter(function (elem, pos) {
        return validSemesters.indexOf(elem) == pos;
    });
    let select_department = document.querySelector('#select_semester')
    // console.log('Filter Semesters')
    for (let i = 0; i < validSemesters.length; i++) {
        // console.log(`${validSemesters[i]}`)
        let content = `<option value="${validSemesters[i]}">${validSemesters[i]}</option>`
        select_semester.innerHTML += content
    }
}
function updateCoursesForm(courses) {
    updateSelectDepartments(courses)
    updateSelectSemesters(courses)
}
function filterCourses(arrayCourses, value, property = 'department') {
    return arrayCourses.filter(course => course[property].toLowerCase() == value.toLowerCase())
}
function formCourseSubmission(e) {
    e.preventDefault()
    const departmentInput = document.getElementById('select_department')
    const semesterInput = document.getElementById('select_semester')
    const departmentValue = departmentInput.value
    const semesterValue = semesterInput.value
    console.log(`Department: ${departmentValue} Semester: ${semesterValue}`)
    let filtered
    let departmentFiltered = []
    let semesterFiltered = []
    if (departmentValue == '' && semesterValue == '') {
        alert('Please select a value to filter!')
        return
    }
    if (departmentValue != '') {
        filtered = filterCourses(coursesArray, departmentValue)
    }
    if (semesterValue != '') {
        filtered = filterCourses(coursesArray, semesterValue, 'semester')
    }
    updateCourseDisplay(filtered)
    departmentInput.selectedIndex = 0
    semesterInput.selectedIndex = 0
}

/*
    Student Related Functions
*/
function formStudentSubmission(e) {
    e.preventDefault()
    let student_name = document.getElementById('student_name')
    let student_major = document.getElementById('student_major')
    let student_email = document.getElementById('student_email')
    let student_gpa = document.getElementById('student_gpa')
    let fake_courses = ['Coding', 'Thinking']
    let errorMessage = ''
    if (student_name.value == '') {
        errorMessage += `<p>Please enter the student's name.</p>`
        student_name.classList.add('error')
    }
    if (student_major.value == '') {
        errorMessage += `<p>Please enter the student's major.</p>`
        student_major.classList.add('error')
    }
    if (!validateEmail(student_email.value)) {
        errorMessage += `<p>Please enter a valid email.</p>`
        student_email.classList.add('error')
    }
    if (student_gpa.value == '') {
        errorMessage += `<p>Please enter the student's GPA.</p>`
        student_gpa.classList.add('error')
    }
    if (student_gpa.value > 5) {
        errorMessage += `<p>Please enter a GPA lower than 5.0.</p>`
        student_gpa.classList.add('error')
    }
    if (student_gpa.value < 0) {
        errorMessage += `<p>Please enter a GPA more than 0.0.</p>`
        student_gpa.classList.add('error')
    }
    if (errorMessage != '') {
        let errorContent = `<div class="error-message">${errorMessage}</div>`
        document.querySelector('.student_form').innerHTML += errorContent
        return
    }

    let newStudent = new Student(student_name.value, student_major.value, student_email.value, student_gpa.value, fake_courses)
    studentsArray.push(newStudent)
    updateStudentsDisplay(studentsArray)
    student_name.value = student_major.value = student_email.value = student_gpa.value = ''
}

function updateStudentsDisplay(students) {
    let students_container = document.querySelector('.student_list')
    // console.log('List of students')
    students_container.innerHTML = ''
    for (let i = 0; i < students.length; i++) {
        // console.log(`${students[i].name}, ${students[i].major}`)
        let content = `
            <article class="student">
                <header class="student__header">
                    <img class="student__thumbnail thumbnail" src="img/profile_thumb.png" alt="Sally Jones" />
                    <h2 class="student__name">${students[i].name}</h2>
                    <h3 class="student__major">${students[i].major}</h3>
                </header>
                <aside class="student__detail">
                    <div class="student__detail_row detail_row">
                        <h4 class="student__email_title">Email:</h4>
                        <p class="student__email"><a href="mailto:${students[i].email}">${students[i].email}</a></p>
                    </div>
                    <div class="student__detail_row detail_row">
                        <h4 class="student__gpa_title">GPA:</h4>
                        <p class="student__gpa">${students[i].gpa}</p>
                    </div>
                    <div class="student__detail_row detail_row">
                        <h4 class="student__courses_title">Courses:</h4>
                        <ul class="student__courses_list course_list">
                            <li>${students[i].courses[0]}</li>
                            <li>${students[i].courses[1]}</li>
                        </ul>
                    </div>
                </aside>
            </article>`
        students_container.innerHTML += content
    }
}

/*
    Individual Course Related Functions
*/
let loadedCourse
function updateIndividualCourseForm() {
    let select_student = document.querySelector('#select_student')
    for (let i = 0; i < studentsArray.length; i++) {
        let content = `<option value="${studentsArray[i].name}">${studentsArray[i].name}</option>`
        select_student.innerHTML += content
    }
}
function updateIndividualCourseDisplay(course){
    let courseName = document.querySelector('.course_info .name')
    courseName.innerHTML = course.name
    let courseDepartment = document.querySelector('.course_info .department')
    courseDepartment.innerHTML = course.department
    let courseSemester = document.querySelector('.course_info .semester')
    courseSemester.innerHTML = course.semester
    let courseTeacher = document.querySelector('.course_info .teacher_name')
    courseTeacher.innerHTML = course.teacher.name

    let student_list = document.querySelector('.student_list')
    student_list.innerHTML = ''
    for (let i = 0; i < course.students.length; i++) {
        let content = `<h3 class="student_name">${course.students[i].name}</h3>`
        student_list.innerHTML += content
    }
    if (!course.students.length) {
        let content = `<h3 class="student_name">No Students Registered!</h3>`
        student_list.innerHTML = content
    }
}
function formIndieCourseSubmission(e) {
    e.preventDefault()
    let studentInput = document.getElementById('select_student')
    let loadedStudent = studentsArray.find(item => item.name === studentInput.value)
    studentInput.value = ''
    loadedCourse.addStudent(loadedStudent)
    updateIndividualCourseDisplay(loadedCourse)
}
/*
    Init Index Page Scripts
*/
if (document.querySelector(".homepage_content")) {
    setTimeout(function () {
        toggleLoading()
    }, 500)
}
/*
    Init Courses Page Scripts
*/
if (document.querySelector(".courses_content")) {
    setTimeout(function () {
        toggleLoading()
    }, 500)
    updateCourseDisplay(coursesArray)
    updateCoursesForm(coursesArray)
    document.getElementById("form_courses_filter").addEventListener("submit", formCourseSubmission)
}

/*
    Init Teacher Page Scripts
*/
if (document.querySelector(".teachers_content")) {
    setTimeout(function () {
        toggleLoading()
    }, 500)
    updateTeachersDisplay(teachersArray)
    document.getElementById("form_teachers_rating").addEventListener("submit", formTeacherSubmission)
}

/*
   Init Student Page Scripts
*/
if (document.querySelector(".student_content")) {
    setTimeout(function () {
        toggleLoading()
    }, 500)
    updateSelectDepartments(coursesArray)
    updateStudentsDisplay(studentsArray)
    document.getElementById("form_student_create").addEventListener("submit", formStudentSubmission)
}

/*
   Init Individual Course Page Scripts
*/
if (document.querySelector(".course_content")) {
    setTimeout(function () {
        toggleLoading()
    }, 500)
    let current_url = window.location.pathname.split('/').pop().replace('.html', '').replace('-', ' ').replace('-', ' ')
    loadedCourse = coursesArray.find(item => item.name.toLowerCase() === current_url)
    updateIndividualCourseDisplay(loadedCourse)
    updateIndividualCourseForm()
    document.getElementById("form_course_roster").addEventListener("submit", formIndieCourseSubmission)
}