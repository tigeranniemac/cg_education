console.log("all systems go!")

/*
Challenge 1: Writing Anonymous Functions
STEP 1: Prompting User
1. Student’s college graduation year (2018) 
2. Student’s college graduation month (May or December). 
If the user’s input is not valid, remember to re-prompt them
*/
// var graduationYearPromptMessage = `What year are you graduating?`
// var collegeGraduationYear = prompt(graduationYearPromptMessage)
// collegeGraduationYear = parseInt(collegeGraduationYear)

// while (collegeGraduationYear < 2018 || isNaN(collegeGraduationYear) ) {
//     var errorMessage = `\nPlease enter a year either 2018 or after.`
//     collegeGraduationYear = prompt(`${graduationYearPromptMessage} ${errorMessage}`)
// }

// var validMonths = ['May', 'December']
// var validMonthsLower = []
// validMonths.forEach(month => validMonthsLower.push(month.toLowerCase()))
// var graduationMonthPromptMessage = `What month are you graduating?`
// var graduationMonth = prompt(graduationMonthPromptMessage)
// while (validMonthsLower.indexOf(graduationMonth.toLowerCase()) == -1) {
//     var errorMessage = `\nPlease enter either May or December.`
//     graduationMonth = prompt(`${graduationMonthPromptMessage} ${errorMessage}`)
// }

/*
STEP 2: Figuring Out if the User is in High School or College
1. use the college grad year to figure out if the student is currently in high school or college…
2. if in college, write to the console, “You are in college.”.
3. if in high school, write to the console, “You are in high school”.
*/
// function studentType(year) {
//     var message
//     switch (year) {
//         case 2018 : 
//         case 2019 : 
//         case 2020 : 
//         case 2021 : 
//             message = `You are in college.`
//             break;
//         case 2022 : 
//         case 2023 : 
//         case 2024 : 
//         case 2025 : 
//             message = `You are in high school.`
//             break;
//         default :
//             message = `Error in finding type of student you are.`

//     }
//     return message
// }
// var getStudentType = studentType(collegeGraduationYear)
// console.log(getStudentType)

/*
STEP 3: Welcoming Some Students 
1. Write 2 anonymous functions and store then in variables called… welcomeCollegeStudent and welcomeHsStudent. 
2. Each will take one parameter, the student’s class (like Freshman, Sophomore… etc).

The college welcome function should alert the following message…
"Welcome <students-class>!"

The high school welcome function should alert the following message…
"You're still a <students-class> in high school!"
Replace the console logs in these above steps with calling these functions! 
For now, just use a “Freshman” parameter. Our next step will be to figure out which class the college or high school student is in.
*/

var welcomeCollegeStudent = function (studentClass) {
    studentClass = studentClass.charAt(0).toUpperCase() + studentClass.substr(1);
    console.log(`Welcome ${studentClass}!`)
};
var welcomeHSStudent = function (studentClass) {
    studentClass = studentClass.charAt(0).toUpperCase() + studentClass.substr(1);
    console.log(`You're still a ${studentClass} in high school!`)
};
// function studentType(year) {
//     var message
//     switch (year) {
//         case 2018:
//             welcomeCollegeStudent("Senior")
//             break;
//         case 2019:
//             welcomeCollegeStudent("Junior")
//             break;
//         case 2020:
//             welcomeCollegeStudent("Sophomore")
//             break;
//         case 2021:
//             welcomeCollegeStudent("Freshman")
//             break;
//         case 2022:
//             welcomeHSStudent("Senior")
//             break;
//         case 2023:
//             welcomeHSStudent("Junior")
//             break;
//         case 2024:
//             welcomeHSStudent("Sophomore")
//             break;
//         case 2025:
//             welcomeHSStudent("Freshman")
//             break;
//         default:
//             console.log(`Error in finding type of student you are.`)

//     }
// }
// studentType(collegeGraduationYear)
// console.log(getStudentType)

/*
STEP 4: Figuring out the student’s class
welcomeStudentByGraduatingClass should use the grad month and year to figure out what graduating class: Freshman, Sophomore, etc
then call the welcome function and pass the correct graduating class as a parameter to that welcome function.
*/

var whoaNelly = function (year) {
    console.log(`${year}... whoa, college is some years away...`)
};
function studentType(year, month) {
    switch (year) {
        case 2018:
            welcomeStudentByGraduatingClass(year, month, welcomeCollegeStudent("Senior"))
            break;
        case 2019:
            welcomeStudentByGraduatingClass(year, month, welcomeCollegeStudent("Junior"))
            break;
        case 2020:
            welcomeStudentByGraduatingClass(year, month, welcomeCollegeStudent("Sophomore"))
            break;
        case 2021:
            welcomeStudentByGraduatingClass(year, month, welcomeCollegeStudent("Freshman"))
            break;
        case 2022:
            welcomeStudentByGraduatingClass(year, month, welcomeHSStudent("Senior"))
            break;
        case 2023:
            welcomeStudentByGraduatingClass(year, month, welcomeHSStudent("Junior"))
            break;
        case 2024:
            welcomeStudentByGraduatingClass(year, month, welcomeHSStudent("Sophomore"))
            break;
        case 2025:
            welcomeStudentByGraduatingClass(year, month, welcomeHSStudent("Freshman"))
            break;
        default:
            welcomeStudentByGraduatingClass(year, month, whoaNelly(year))
    }
}
function welcomeStudentByGraduatingClass(gradMonth, gradYear, welcome) {
    console.log(`gradMonth: ${gradMonth} gradYear: ${gradYear} welcome: ${welcome} `)
    console.log("ok")
}
// studentType(collegeGraduationYear, graduationMonth)
// console.log(getStudentType)

/*
STEP 1: Creating Your First Object: the Teacher Object
name : string
department : string
ratings : array
addRating : function
getAvgRating : function
STEP 2: Using Your Teacher Object
Reuse prompts to change teacher object
*/
// var teacher = {
//     name: 'Sally Jones',
//     department: 'Physics',
//     ratings: [3.4, 5.0, 4.2],
//     addRating: function (newRating) {
//         this.ratings.push(parseFloat(newRating))
//     },
//     getAvgRating: function() {
//         var total = 0
//         for (var i = 0; i < this.ratings.length; i++) {
//             total += this.ratings[i]
//         }
//         this.avgRating = total / this.ratings.length
//         this.avgRating = Math.round(this.avgRating * 10) / 10
//     }
// }
// teacher.getAvgRating()
// console.log(teacher.avgRating)

// var teacherReviewMessage = `We would like for you to review ${teacher.name}. Please enter a rating between 0.0 - 5.0?`
// var teacherReview = prompt(teacherReviewMessage)

// while (teacherReview > 5) {
//     teacherReview = prompt(`${teacherReviewMessage}. Please choose between 0.0 and 5.0.`)
// }

// teacher.getAvgRating()
// var teacherThankYou = `Thanks for you review! ${teacher.name} average rating is now ${teacher.avgRating}.`
// console.log(teacherThankYou)

/*
STEP 3: Create A Course Object
name
department
teacher
semester
STEP 4: Fix the filterCourses function
STEP 5: Adding an object as a property of another object
*/
// var course = {
//     name: 'Basketball',
//     department: 'Physical Education',
//     teacher: teacher,
//     semester: 'Fall'
// }
// var coursesArray = [
//     {
//         name: 'Basketball',
//         department: 'Physical Education',
//         teacher: teacher,
//         semester: 'Fall'
//     },
//     {
//         name: 'Softball',
//         department: 'Physical Education',
//         teacher: teacher,
//         semester: 'Fall'
//     },
//     {
//         name: 'Track',
//         department: 'Physical Education',
//         teacher: teacher,
//         semester: 'Fall'
//     },
//     {
//         name: 'Drawing',
//         department: 'Art',
//         teacher: teacher,
//         semester: 'Fall'
//     },
//     {
//         name: 'Painting',
//         department: 'Art',
//         teacher: teacher,
//         semester: 'Fall'
//     },
//     {
//         name: 'Ceramics',
//         department: 'Art',
//         teacher: teacher,
//         semester: 'Fall'
//     },
//     {
//         name: 'World War I',
//         department: 'History',
//         teacher: teacher,
//         semester: 'Fall'
//     },
//     {
//         name: 'World War II',
//         department: 'History',
//         teacher: teacher,
//         semester: 'Fall'
//     },
//     {
//         name: 'War on Drugs',
//         department: 'History',
//         teacher: teacher,
//         semester: 'Fall'
//     }
// ]
// function filterCourses(arrayCourses, department) {
//     return arrayCourses.filter(course => course.department.toLowerCase() == department.toLowerCase())
// }
// var filtered = filterCourses(coursesArray, "art")
// console.log('filtered')
// console.log(filtered)
// console.log(filtered[0].teacher.name)

/*
STEP 1: Turn the Teacher Object into an Re-usable Object Prototype
STEP 2: Create new Teacher Instances
*/

function Teacher(name, department, ratings) {
    this.name = name
    this.department = department
    this.ratings = ratings
    this.avgRating = 0
}
Teacher.prototype.addRating = function(newRating) {
    this.ratings.push(parseFloat(newRating))
}
Teacher.prototype.getAvgRating = function() {
    this.avgRatingOld = this.avgRating
    var total = 0
    for (var i = 0; i < this.ratings.length; i++) {
        total += this.ratings[i]
    }
    var avgRating = total / this.ratings.length
    avgRating = Math.round(avgRating * 10) / 10
    this.avgRating = avgRating
}
Teacher.prototype.niceTeacher = function (teacherReview) {
    var niceTeacher = `${this.name} 
${this.department} 
Avg. Rating: ${this.avgRatingOld}
Adding Rating: ${teacherReview}
New Avg Rating: ${this.avgRating}`
    console.log(niceTeacher)
}

//Teacher: Bob Ross
// var teacherBobRoss = new Teacher('Bob Ross', 'Art', [3.4, 5.0, 4.2]);
// teacherBobRoss.getAvgRating()

// var teacherReviewMessage = `We would like for you to review ${teacherBobRoss.name}. Please enter a rating between 0.0 - 5.0?`
// var teacherReview = prompt(teacherReviewMessage)

// while (teacherReview > 5) {
//     teacherReview = prompt(`${teacherReviewMessage}. Please choose between 0.0 and 5.0.`)
// }
// teacherBobRoss.addRating(teacherReview)
// teacherBobRoss.getAvgRating()
// var teacherThankYou = `Thanks for you review! ${teacherBobRoss.name} average rating is now ${teacherBobRoss.avgRating}.`
// console.log(teacherThankYou)
// teacherBobRoss.niceTeacher(teacherReview)

// //Teacher: Yoda
// var teacherYoda = new Teacher('Yoda', 'Jedi', [4.9, 3.7, 2.6]);
// teacherYoda.getAvgRating()

// var teacherReviewMessage = `We would like for you to review ${teacherYoda.name}. Please enter a rating between 0.0 - 5.0?`
// var teacherReview = prompt(teacherReviewMessage)

// while (teacherReview > 5) {
//     teacherReview = prompt(`${teacherReviewMessage}. Please choose between 0.0 and 5.0.`)
// }
// teacherYoda.addRating(teacherReview)
// teacherYoda.getAvgRating()
// var teacherThankYou = `Thanks for you review! ${teacherYoda.name} average rating is now ${teacherYoda.avgRating}.`
// console.log(teacherThankYou)
// teacherYoda.niceTeacher(teacherReview)

// //Teacher: Janelle Monae
// var teacherJanelle = new Teacher('Janelle Monae', 'Music', [5.0, 4.6, 3.8]);
// teacherJanelle.getAvgRating()

// var teacherReviewMessage = `We would like for you to review ${teacherJanelle.name}. Please enter a rating between 0.0 - 5.0?`
// var teacherReview = prompt(teacherReviewMessage)

// while (teacherReview > 5) {
//     teacherReview = prompt(`${teacherReviewMessage}. Please choose between 0.0 and 5.0.`)
// }
// teacherJanelle.addRating(teacherReview)
// teacherJanelle.getAvgRating()
// var teacherThankYou = `Thanks for you review! ${teacherJanelle.name} average rating is now ${teacherJanelle.avgRating}.`
// console.log(teacherThankYou)
// teacherJanelle.niceTeacher(teacherReview)


/*
STEP 3: Create Course Prototype
*/
function Course (name, department, teacher, semester) {
    this.name = name
    this.department = department
    this.teacher = teacher
    this.semester = semester
}

var coursesArray = []

var patSummit = new Teacher('Pat Summitt', 'Basketball', [5.0, 5.0, 5.0]);
var introBasketball = new Course('Intro to Basketball', 'Physical Education', patSummit, 'Fall');
coursesArray.push(introBasketball);
var jvBasketball = new Course('Junior Basketball', 'Physical Education', patSummit, 'Spring');
coursesArray.push(jvBasketball);
var varsityBasketball = new Course('Varsity Basketball', 'Physical Education', patSummit, 'Fall');
coursesArray.push(varsityBasketball);
var proBasketball = new Course('Pro Basketball', 'Physical Education', patSummit, 'Spring');
coursesArray.push(proBasketball);

var bobRoss = new Teacher('Bob Ross', 'Art', [3.4, 5.0, 4.2]);
var drawing = new Course('Drawing', 'Art', bobRoss, 'Fall');
coursesArray.push(drawing);
var painting = new Course('Painting', 'Art', bobRoss, 'Spring');
coursesArray.push(painting);

console.log(coursesArray)


function filterCourses(arrayCourses, department) {
    return arrayCourses.filter(course => course.department.toLowerCase() == department.toLowerCase())
}
var filtered = filterCourses(coursesArray, "art")
console.log('filtered')
console.log(filtered)

/*
STEP 4: Create A Student Prototype
name, major, email, avgGPA, courses array
addCourse - function - takes a course parameters, and adds it to the Student’s courses array.
dropCourse - function - takes a course parameter and finds it in the course array and removes it.
changeMajor - function takes a string parameter and changes the Student’s major property
*/
function Student(name, major, email, avgGPA, courses) {
    this.name = name
    this.major = major
    this.email = email
    this.avgGPA = avgGPA
    this.courses = courses
}
Student.prototype.addCourse = function(course) {
    this.courses.push(course)
}
Student.prototype.dropCourse = function(course) {
    let index = this.courses.indexOf(course)
    if (index !== -1) {
        this.courses.splice(index, 1);
    }
}
Student.prototype.changeMajor = function(major) {
    this.major = major
}
Student.prototype.printNice = function (course) {
    var niceCourses = []
    this.courses.filter(course => niceCourses.push(course.name))

    var nicePrint = `Adding Course: ${course.name}
To Student's Courses: ${this.name}
Now they are taking.... ${niceCourses.join(`, `)}`
    console.log(nicePrint)
}

/*
STEP 5: Using the Student Prototype
Adding Course: <course name>
To Student's Courses: <student name>
Now they are taking....
<list course names of all courses the student is taking>
*/

var annieMcCance = new Student('Annie McCance', 'Art', 'me@annie.com', 4.3, [introBasketball, jvBasketball]);
annieMcCance.addCourse(varsityBasketball)
annieMcCance.printNice(varsityBasketball)