# CG Education

CoderGirl 2018's Flexbox and Javascript exercises.

* Flexbox based layout with responsive web design and animations
* Vanilla JS based functionality with loading & error states

* [CG Education 1](https://ktmathews89.github.io/cg_winter_2018_instruction/cg_ed_1)
* [CG Education 2](https://ktmathews89.github.io/cg_winter_2018_instruction/cg_ed_2)
* [CG Education 3](https://ktmathews89.github.io/cg_winter_2018_instruction/cg_ed_3)
* [CG Education Final](https://ktmathews89.github.io/cg_winter_2018_instruction/cg_ed_final)

### Author ###

* Annie McCance, anniemccance@gmail.com